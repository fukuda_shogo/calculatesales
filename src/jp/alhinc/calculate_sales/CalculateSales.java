package jp.alhinc.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CalculateSales {

	// 支店定義ファイル名の定数
	private static final String FILE_NAME_BRANCH_LST = "branch.lst";

	//商品定義ファイル名の定数
	private static final String FILE_NAME_COMMODITY_LST = "commodity.lst";

	// 支店別集計ファイル名の定数
	private static final String FILE_NAME_BRANCH_OUT = "branch.out";

	//商品別集計ファイル名の定数
	private static final String FILE_NAME_COMMODITY_OUT = "commodity.out";

	// エラーメッセージの定数
	private static final String UNKNOWN_ERROR = "予期せぬエラーが発生しました";

	private static final String RECORD_FILE_NOT_SERIAL_NUMBER = "売上ファイル名が連番になっていません";

	private static final String TOTAL_AMOUNT_OVER_TEN_DIGITS = "合計金額が10桁を超えました";

	private static final String ERROR_OF_CODE = "コードが不正です";
	private static final String ERROR_OF_FORMAT = "のフォーマットが不正です";
	private static final String NOT_EXIST_MESSAGE = "が存在しません";

	//正規表現(上から支店定義ファイル、商品定義ファイル、売上ファイル、売上金額）の定数
	private static final String SALES_FILE_REGULAR_EXPRESSIONS = "^[0-9]{8}.rcd$";
	private static final String SALES_AMOUNT = "^[0-9]+$";

	/**
	 * メインメソッド
	 *
	 * @param コマンドライン引数
	 */

	public static void main(String[] args) {
		if(args.length != 1) {
			System.out.println(UNKNOWN_ERROR);
			return;
		}

		// 支店コードと支店名を保持するMap
		Map<String, String> branchNames = new HashMap<>();

		// 支店コードと売上金額を保持するMap
		Map<String, Long> branchSales = new HashMap<>();

		// 商品コードと商品名を保持するMap
		Map<String, String> commodityNames = new HashMap<>();

		// 商品コードと売上金額を保持するMap
		Map<String, Long> commoditySales = new HashMap<>();

		// 支店定義ファイル読み込み処理
		if(!readFile(args[0], FILE_NAME_BRANCH_LST, branchNames, branchSales, "支店定義ファイル", "^[0-9]{3}$")) {
			return;
		}

		// 商品定義ファイル読み込み処理
		if(!readFile(args[0], FILE_NAME_COMMODITY_LST,commodityNames, commoditySales, "商品定義ファイル", "^[A-Za-z0-9]{8}$")) {
			return;
		}

		// ※ここから集計処理を作成してください。(処理内容2-1、2-2)
		// フォルダのファイルを全て読み込む
		File[] files = new File(args[0]).listFiles();

		List<File> rcdFiles = new ArrayList<>();

		// 売上ファイル名が合っているか確認
		for(int i = 0; i < files.length; i++) {
			String fileName = files[i].getName();
			if((files[i].isFile()) && (fileName.matches(SALES_FILE_REGULAR_EXPRESSIONS))) {
				rcdFiles.add(files[i]);
			}
		}

		Collections.sort(rcdFiles);

		// 売上ファイル名が連番になっているか確認（エラー処理）
		for(int i = 0; i < rcdFiles.size() - 1; i++) {
			File beforeFile = rcdFiles.get(i);
			String before = beforeFile.getName();

			File rearFile = rcdFiles.get(i + 1);
			String rear = rearFile.getName();

			int former = Integer.parseInt(before.substring(0, 8));
			int latter = Integer.parseInt(rear.substring(0, 8));

			if((latter - former) != 1) {
				System.out.println(RECORD_FILE_NOT_SERIAL_NUMBER);
				return;
			}
		}

		// 売上ファイル名を読み込み、各支店と各商品に金額を足す
		for(int i = 0; i < rcdFiles.size(); i++) {
			List<String> rcdData = new ArrayList<>();

			BufferedReader br = null;

			try {
				File file = rcdFiles.get(i);
				FileReader fr = new FileReader(file);
				br = new BufferedReader(fr);

				String line;

				while((line = br.readLine()) != null) {
					rcdData.add(line);
				}

				// 売上ファイルのフォーマットを確認（エラー処理）
				if(rcdData.size() != 3) {
					System.out.println((rcdFiles.get(i)).getName() + ERROR_OF_FORMAT);
					return;
				}

				// 売上金額が数字なのか確認（エラー処理）
				if(!(rcdData.get(2).matches(SALES_AMOUNT))) {
					System.out.println(UNKNOWN_ERROR);
					return;
				}

				// Mapに支店のkeyが存在するか確認（エラー処理）
				if(!branchNames.containsKey(rcdData.get(0))){
					System.out.println((rcdFiles.get(i)).getName() + "の支店" + ERROR_OF_CODE);
					return;
				}

				//Mapに商品のkeyが存在するか確認（エラー処理）
				if(!commodityNames.containsKey(rcdData.get(1))) {
					System.out.println((rcdFiles.get(i)).getName() + "の商品" + ERROR_OF_CODE);
					return;
				}

				long fileSale = Long.parseLong(rcdData.get(2));
				Long saleAmount = branchSales.get(rcdData.get(0)) + fileSale;
				Long commodityAmount = commoditySales.get(rcdData.get(1)) + fileSale;

				// 売上金額の合計が10桁を超えたか確認（エラー処理）
				if((saleAmount >= 1000000000) || (commodityAmount >= 1000000000)) {
					System.out.println(TOTAL_AMOUNT_OVER_TEN_DIGITS);
					return;
				}

				branchSales.put((rcdData.get(0)), saleAmount);
				commoditySales.put((rcdData.get(1)), commodityAmount);

			} catch(IOException e) {
				System.out.println(UNKNOWN_ERROR);
				return;

			} finally {
				if(br != null) {
					try {
						br.close();
					} catch(IOException e) {
						System.out.println(UNKNOWN_ERROR);
						return;
					}
				}
			}
		}

		// 支店別集計ファイル書き込み処理
		if(!writeFile(args[0], FILE_NAME_BRANCH_OUT, branchNames, branchSales)) {
			return;
		}

		// 商品別集計ファイル書き込み処理
		if(!writeFile(args[0], FILE_NAME_COMMODITY_OUT, commodityNames, commoditySales)) {
			return;
		}
	}

	/**
	 * 支店定義ファイル読み込み処理
	 *
	 * @param フォルダパス
	 * @param ファイル名
	 * @param 支店コードと支店名を保持するMap
	 * @param 支店コードと売上金額を保持するMap
	 * @return 読み込み可否
	 */

	private static boolean readFile(String path, String fileName, Map<String, String> Names, Map<String, Long> Sales, String errorFileName, String regularExpressions) {
		BufferedReader br = null;

		try {
			File file = new File(path, fileName);
			// ファイルの存在を確認する（エラー処理）
			if(!file.exists()) {
				System.out.println(errorFileName + NOT_EXIST_MESSAGE);
				return false;
			}

			FileReader fr = new FileReader(file);
			br = new BufferedReader(fr);

			String line;

			// 一行ずつ読み込む
			while((line = br.readLine()) != null) {
				// ※ここの読み込み処理を変更してください。(処理内容1-2)
				String[] items = line.split(",");
				// 定義ファイルのフォーマットを確認（エラー処理）
				if((items.length != 2) || (!items[0].matches(regularExpressions))) {
					System.out.println(fileName + ERROR_OF_FORMAT);
					return false;
				}

				Names.put(items[0], items[1]);
				Sales.put(items[0], 0L);
			}

		} catch(IOException e) {
			System.out.println(UNKNOWN_ERROR);
			return false;

		} finally {
			// ファイルを開いている場合
			if(br != null) {
				try {
					// ファイルを閉じる
					br.close();
				} catch(IOException e) {
					System.out.println(UNKNOWN_ERROR);
					return false;
				}
			}
		}

		return true;
	}

	/**
	 * 支店別集計ファイル書き込み処理
	 *
	 * @param フォルダパス
	 * @param ファイル名
	 * @param 支店コードと支店名を保持するMap
	 * @param 支店コードと売上金額を保持するMap
	 * @return 書き込み可否
	 */

	private static boolean writeFile(String path, String fileName, Map<String, String> names, Map<String, Long> Sales) {
		// ※ここに書き込み処理を作成してください。(処理内容3-1)
		BufferedWriter bw = null;

		try {
			File newFile = new File(path, fileName);

			FileWriter fw = new FileWriter(newFile);
			bw = new BufferedWriter(fw);

			for(String key : names.keySet()) {
				String code = names.get(key);
				Long sales = Sales.get(key);
				bw.write(key + "," + code + "," + sales);
				bw.newLine();
			}

		} catch(IOException e) {
			System.out.println(UNKNOWN_ERROR);
			return false;

		} finally {
			if(bw != null) {
				try {
					bw.close();
				} catch(IOException e) {
					System.out.println(UNKNOWN_ERROR);
					return false;
				}
			}
		}

		return true;
	}
}
